package com.covisint.transformation.serviceimpl;

import org.springframework.stereotype.Service;

import com.covisint.transformation.service.PostProcessor;

@Service("postProcessor")
public class PostProcessorImpl implements PostProcessor {

	public String callPostProcessor(String senderWirePayload) {
		// TODO: We can add any data/value here as per the requirements

		// TODO: Add Audit/Log
		
		return senderWirePayload;
	}

}
