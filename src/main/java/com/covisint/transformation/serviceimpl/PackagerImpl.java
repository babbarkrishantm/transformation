package com.covisint.transformation.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.covisint.transformation.beans.DataHubMessage;
import com.covisint.transformation.service.Packager;
import com.covisint.transformation.util.JSONUtil;

@Service("packager")
public class PackagerImpl implements Packager {
	@Autowired
	JSONUtil jsonUtil;  
	public String callPackager(DataHubMessage datahubMessage) {		
		try {
			if (datahubMessage != null) {
				return jsonUtil.convertObjectToJSON(datahubMessage);
			}
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
