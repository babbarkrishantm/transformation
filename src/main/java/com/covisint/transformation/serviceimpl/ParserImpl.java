package com.covisint.transformation.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.covisint.transformation.beans.RawMessage;
import com.covisint.transformation.service.Parser;
import com.covisint.transformation.util.StringUtil;

@Service("parser")
public class ParserImpl implements Parser {

	@Autowired
	StringUtil stringUtil;
	
	public RawMessage callParser(String message) {
		
		RawMessage rawMessage = stringUtil.splitDataIntoMessage(message);
		
		// TODO: Add Audit/Log

		return rawMessage;		
	}

}
