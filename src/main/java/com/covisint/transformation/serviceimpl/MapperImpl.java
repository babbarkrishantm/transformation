package com.covisint.transformation.serviceimpl;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.covisint.transformation.beans.DataHubMessage;
import com.covisint.transformation.beans.RawMessage;
import com.covisint.transformation.beans.SenderWirePayload;
import com.covisint.transformation.service.Mapper;

@Service("mapper")
public class MapperImpl implements Mapper {

	public DataHubMessage callMapper(RawMessage rawMessage) {
		if (rawMessage != null) {
			return mapDataHubMessage(rawMessage);
		}
		return null;
	}

	private DataHubMessage mapDataHubMessage(RawMessage rawMessage) {
		DataHubMessage dataHubMessage = new DataHubMessage();
		String deviceId = rawMessage.getDeviceId();	// We can call any external service to get device id mapped on IoT platform
		String eventTemplateId = "5a935d84-e0e0-4357-9c90-2189498a6b7f"; // Get Event Template information from external source (IoT Plateform)
		
		dataHubMessage.setDeviceId(deviceId);		
		dataHubMessage.setCustomerMessageID(rawMessage.getCustomerMessageID());
		dataHubMessage.setEventTemplateId(eventTemplateId);		
		dataHubMessage.setRealm(rawMessage.getRealm());
		
		SenderWirePayload senderWirePayload = new SenderWirePayload();
		senderWirePayload.setCreation(new Timestamp((new Date()).getTime()).getTime());
		senderWirePayload.setCreator("IOTADMIN");
		senderWirePayload.setDeviceId(deviceId);
		senderWirePayload.setEncodingType("BASE64");
		senderWirePayload.setEventTemplateId(eventTemplateId);
		senderWirePayload.setMessage(rawMessage.getMessage());
		senderWirePayload.setMessageId(rawMessage.getCustomerMessageID());
		
		dataHubMessage.setSenderWirePayload(senderWirePayload);
		return dataHubMessage;
	}
}
