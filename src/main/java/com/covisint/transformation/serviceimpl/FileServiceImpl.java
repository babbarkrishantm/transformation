package com.covisint.transformation.serviceimpl;

import org.springframework.stereotype.Service;

import com.covisint.transformation.service.FileService;

@Service("fileService")
public class FileServiceImpl implements FileService {

	public void persistMessage(String message) {
		System.out.println("SenderWirePayload JSON: " + message);		
	}
	
}
