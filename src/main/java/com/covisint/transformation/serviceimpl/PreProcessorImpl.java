package com.covisint.transformation.serviceimpl;

import org.springframework.stereotype.Service;

import com.covisint.transformation.service.PreProcessor;

@Service("preProcessor")
public class PreProcessorImpl implements PreProcessor {

	public String callPreProcessor(String message) {
		// TODO: We can add any data/value here as per the requirements

		// TODO: Add Audit/Log
		return message;
	}

}
