package com.covisint.transformation;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import com.covisint.transformation.util.FileUtil;

public class Application {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// load the Spring context
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
 
        // get the reference to the message channel
        MessageChannel channel = context.getBean("inputChannelPreProcessor", MessageChannel.class);
        
        FileUtil fileUtil = (FileUtil) context.getBean("fileUtil");
        List<String> dataList = fileUtil.getData();
        
        for (String data : dataList) {
            // create a message with the content "World"
            Message<String> message = MessageBuilder.withPayload(data).build();
            // send the message to the inputChannel
            channel.send(message);
		}
	}
}
