package com.covisint.transformation.util;

import org.springframework.stereotype.Component;

import com.covisint.transformation.beans.RawMessage;

@Component
public class StringUtil {

	public final String CSV_SPLIT_BY = ",";
	public final int RAW_MESSAGE_LENGTH = 5;

	public RawMessage splitDataIntoMessage(String data) {
		// use comma as separator
		String[] messageData = data.split(CSV_SPLIT_BY);
		RawMessage rawMessage = null;
		
		if (messageData.length == RAW_MESSAGE_LENGTH) {
			rawMessage = new RawMessage();
			if (messageData[0] != null && !messageData[0].isEmpty()) {
				rawMessage.setRealm(messageData[0]);
			}
			if (messageData[1] != null && !messageData[1].isEmpty()) {
				rawMessage.setDeviceId(messageData[1]);
			}
			if (messageData[2] != null && !messageData[2].isEmpty()) {
				rawMessage.setCommandEventId(messageData[2]);
			}
			if (messageData[3] != null && !messageData[3].isEmpty()) {
				rawMessage.setCustomerMessageID(messageData[3]);
			}
			if (messageData[4] != null && !messageData[4].isEmpty()) {
				rawMessage.setMessage(messageData[4]);
			}
		}
		else {
			// TODO: Add Logger
			System.out.println("Unable to process data because it does not match with the required length: " + RAW_MESSAGE_LENGTH + ". Data is: " + messageData);
		}
		
		return rawMessage;
	}
}
