package com.covisint.transformation.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component("fileUtil")
public class FileUtil {
	// TODO: Read file path from properties file
	public final String SOURCE_DATA_FILE_NAME = "d:/source-data.csv";

	public List<String> getData() {
		List<String> dataList = new ArrayList<String>();
		BufferedReader br = null;
		String line = "";
		
		try {
			br = new BufferedReader(new FileReader(SOURCE_DATA_FILE_NAME));
			while ((line = br.readLine()) != null) {
				dataList.add(line);
			}
			// Temp kludge 
			dataList.remove(0); // Removing header row.
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		return dataList;
	}
}
