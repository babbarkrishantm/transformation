package com.covisint.transformation.beans;

public class RawMessage {
	
	/** The realm of the dataHubMessage resource. */
    private String realm;
    
	/** Device id of the device. */
    private String deviceId;
    
    /** Id of a command/event to send to device. */
    private String commandEventId;
    
    /** Id of a command to send to device. */
    private String customerMessageID;
    
    /** The actual event message payload sent from a device to application. */
    private String message;

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getCommandEventId() {
		return commandEventId;
	}

	public void setCommandEventId(String commandEventId) {
		this.commandEventId = commandEventId;
	}

	public String getCustomerMessageID() {
		return customerMessageID;
	}

	public void setCustomerMessageID(String customerMessageID) {
		this.customerMessageID = customerMessageID;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
