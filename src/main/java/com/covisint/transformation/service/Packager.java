package com.covisint.transformation.service;

import com.covisint.transformation.beans.DataHubMessage;

public interface Packager {
	public String callPackager(DataHubMessage datahubMessage);
}
