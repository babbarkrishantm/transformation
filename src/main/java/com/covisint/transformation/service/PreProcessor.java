package com.covisint.transformation.service;

/**
 * This service simply append the addon data in original message and return the new message
 */
public interface PreProcessor {
	public String callPreProcessor(String rawMessage);
}
