package com.covisint.transformation.service;

import com.covisint.transformation.beans.DataHubMessage;
import com.covisint.transformation.beans.RawMessage;

/**
 * This service parse the message and covert into RawMessage and return the new message
 */
public interface Mapper {
	
	public DataHubMessage callMapper(RawMessage rawMessage);	
}
