package com.covisint.transformation.service;

import com.covisint.transformation.beans.RawMessage;

/**
 * This service parse the message and covert into RawMessage and return the new message
 */
public interface Parser {
	public RawMessage callParser(String message);
}
