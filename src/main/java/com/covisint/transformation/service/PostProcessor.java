package com.covisint.transformation.service;

public interface PostProcessor {
	public String callPostProcessor(String senderWirePayload);
}
